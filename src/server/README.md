# IDX API
> A starter project with NodeJs, Express, MongoDB

### IDX API structure

```

├── README.md
├── app
│   ├── controllers
│   ├── helpers
│   ├── locales
│   ├── middlewares
│   ├── models
│   ├── routes
│   ├── services
│   ├── views
│   ├── worker
├── assets
│   ├── css
│   ├── img
│   ├── js
│   ├── upload
│   └── locales
├── bin
├── config
├── docs
└── test
│   ├── integration
│   ├── unit
│   ├── web
│   └── web demo
├── .jscsrc
├── app.js
├── bower.json
├── CHANGELOG.md
├── package.json
├── README.md
└── start
    

```

## Installation

__Install Node Modules__

`npm install`

then run:

```
. start
```

## Author

- Faerul Salamun (faerulsalamun@gmail.com)