13-01-2016
    0.1.0
     - Init base app server
     
18-01-2016
    0.2.0
     - Add dummy for API Domestic, Foreign, and Province
     - Remove unused function and module
     
19-01-2016
    0.3.0
     - Change return object change for buy and sell
     - Fix bug send data for province
     
24-01-2016
    0.4.0
     - Add dummy data for page 5,6 and 7
     
26-01-2016
    0.4.1
     - Change controller for route LQ
     
26-01-2016
    0.4.2
     - Add data dummy for LQ
     
26-01-2016
    0.4.3
     - Limit data dummy for LQ just 45
     
29-01-2016
    0.5.0
     - Domestic, ExcahngeMembersTransaction, LQ, and Province from DB
     
29-01-2016
    0.6.0
     - Add world major index images & positioning

31-01-2016
    0.7.0
     - Change structure province
     
03-02-2016
    0.8.0
     - Foreign from API
     - Change logic volume for Domestic and Province
    
08-02-2016
    0.9.0
     - Add data from latest when date today null