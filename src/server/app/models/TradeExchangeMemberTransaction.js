/**
 * Created by faerulsalamun on 2/16/16.
 */

'use strict';

const mongoose = require(`mongoose`);
const config = require(`../../config/${process.env.NODE_ENV || ``}`);

const timestamp = require(`./plugins/Timestamp`);

const Schema = mongoose.Schema;
const TradeExchangeMemberTransactionSchema = new Schema({

  id: false,

  buy_summary: {
    type: Number,

  },
  sell_summary: {
    type: Number,

  },
  datas: [
    {
      name: {
        type: String,
      },
      rank: {
        type: Number,
      },
      buy: {
        type: Number,
      },
      sell: {
        type: Number,
      },
      diff: {
        type: Number,
      },
      image: {
        type: String,
      },
    },
  ],


}, {
  collection: config.collection.name(`trade_exchange_member_transaction`),
  toObject: {
    virtuals: true,
  },
  toJSON: {
    virtuals: true,
  },
});

TradeExchangeMemberTransactionSchema.plugin(timestamp.useTimestamps);

module.exports = mongoose.model(`TradeExchangeMemberTransaction`, TradeExchangeMemberTransactionSchema);
