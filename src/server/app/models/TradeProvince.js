/**
 * Created by faerulsalamun on 2/16/16.
 */


const mongoose = require(`mongoose`);
const config = require(`../../config/${process.env.NODE_ENV || ``}`);

const timestamp = require(`./plugins/Timestamp`);

const Schema = mongoose.Schema;
const TradeProvinceSchema = new Schema({

  id: false,

  datas: [
    {
      name: {
        type: String,
      },
      lat: {
        type: Number,
      },
      lon: {
        type: Number,
      },
      buy: {
        radius: {
          type: Number,
        },
        value: {
          type: Number,
        },
        change: {
          type: Number,
        },
        volume: {
          type: Number,
        },
      },
      sell: {
        radius: {
          type: Number,
        },
        value: {
          type: Number,
        },
        change: {
          type: Number,
        },
        volume: {
          type: Number,
        },
      },
      labelPositioning: {
        top: {
          type: Number,
        },
        left: {
          type: Number,
        },
      },
      side: {
        type: String,
      },
    },
  ],


}, {
  collection: config.collection.name(`trade_provinces`),
  toObject: {
    virtuals: true,
  },
  toJSON: {
    virtuals: true,
  },
});

TradeProvinceSchema.plugin(timestamp.useTimestamps);

module.exports = mongoose.model(`TradeProvince`, TradeProvinceSchema);
