/**
 * Created by faerulsalamun on 2/16/16.
 */


const mongoose = require(`mongoose`);
const config = require(`../../config/${process.env.NODE_ENV || ``}`);

const timestamp = require(`./plugins/Timestamp`);

const Schema = mongoose.Schema;
const TradeLQSchema = new Schema({

  id: false,

  datas: [
    {
      name: {
        type: String,
      },
      current_value: {
        type: Number,
      },
      previous_value: {
        type: Number,
        default: 0,
      },
      image: {
        type: String,
      },
    },
  ],


}, {
  collection: config.collection.name(`trade_lqs`),
  toObject: {
    virtuals: true,
  },
  toJSON: {
    virtuals: true,
  },
});

TradeLQSchema.plugin(timestamp.useTimestamps);

module.exports = mongoose.model(`TradeLQ`, TradeLQSchema);
