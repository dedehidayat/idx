/**
 * Created by faerulsalamun on 1/24/17.
 */

const _ = require(`lodash`);
const moment = require(`moment-timezone`);

// Services
const Async = require(`../services/Async`);
const Utils = require(`../services/Utils`);

// Models
const TradeLQ = require(`../models/TradeLQ`);

module.exports = {

  getSyncDataApi: Async.route(function* (req, res, next) {
    const dataDay = Utils.getDay();
    const tomorrow = moment(dataDay).endOf(`days`).format(`YYYY-MM-DDTHH:mm:ss`);

    let datas = yield TradeLQ.findOne({
      createdTime: {
        $gte: dataDay,
        $lt: tomorrow,
      },
    }).lean().exec();

    if (!datas) {
      // Get latest data
      datas = yield TradeLQ.findOne()
        .limit(1)
        .sort({ _id: -1 })
        .lean()
        .exec();
    }

    if (datas) {
      return res.ok(datas.datas);
    }

    return res.ok([]);
  }),

};

