const express = require(`express`);
const router = express.Router();

// Controller
const ProvinceController = require(`../controllers/ProvinceController`);
const ForeignController = require(`../controllers/ForeignController`);
const DomesticController = require(`../controllers/DomesticController`);
const ExchangeMembersTransactionController = require(`../controllers/ExchangeMembersTransactionController`);
const LQController = require(`../controllers/LQController`);
const WorldMajorBourseIndexController = require(`../controllers/WorldMajorBourseIndexController`);

router.get(`/`, (req, res, next) => {
  res.ok(`IDX API Development :)`);
});

router.post(`/provinces/sync`, ProvinceController.getSyncDataApi);
router.post(`/domestics/sync`, DomesticController.getSyncDataApi);
router.post(`/foreigns/sync`, ForeignController.getSyncDataApi);
router.post(`/exchange_member_transactions/sync`, ExchangeMembersTransactionController.getSyncDataApi);
router.post(`/lqs/sync`, LQController.getSyncDataApi);
router.post(`/world_major_bourse_indexs/sync`, WorldMajorBourseIndexController.getSyncDataApi);

module.exports = router;
