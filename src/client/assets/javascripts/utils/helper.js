import axios from 'axios'
import config from './config'

export function numberFormat(num, precision) {
  precision = typeof precision == 'undefined' ? 0 : precision

	return parseFloat(num).toFixed(precision).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

export function calculateVolume(value, min, max, extra) {
  var ratio = ((value - min) / (max - min)) * 10

  return (ratio * 0.5) + extra
}

export function CallAPI(path, data) {
  return axios.post(`${config.API_URL}/${path}`, data)
}
