import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';

import foreign from './../features/foreign/reducers'
import domestic from './../features/domestic/reducers'
import province from './../features/province/reducers'
import exchangeMember from './../features/exchangeMember/reducers'
import LQ45 from './../features/LQ45/reducers'
import asean from './../features/asean/reducers'

export default combineReducers({
  routing,
  foreign,
  domestic,
  province,
  exchangeMember,
  LQ45,
  asean
});
