import React, { Component } from 'react'
import { geoPath, select, geoEquirectangular, mouse, geoBounds, geoMercator, geoCentroid } from 'd3'
import { feature, mesh } from 'topojson'
import WorldJSON from 'json!./countries.topojson'

export default class World extends Component {
  static childContextTypes = {
    projection: React.PropTypes.any.isRequired
  };

  constructor(props) {
    super(props)

    this.projection = function() {
      return null
    }
  }
  getChildContext() {
    return {
      projection: this.projection
    }
  }

  componentWillMount() {
    this.updateDimensions()
  }

  componentDidMount() {
    this.draw()
  }

  updateDimensions() {
    this.setState({
      width: window.innerWidth,
      height: window.innerHeight
    });
  }

  draw() {
    let offsetTop = this.props.offsetTop || 75
    var svg1 = select("#map-wrapper").append("svg")
      .attr("width", this.state.width)
      .attr("height", this.state.height);

    this.projection = geoMercator()
      .scale((this.state.width * (this.state.height / this.state.width)) / Math.PI)
      .translate([this.state.width / 2, (this.state.height / 2) + offsetTop])
      .rotate([this.props.offsetX || -169, 0])

    var world_path = geoPath()
      .projection(this.projection);

    svg1
      .selectAll("path")
      // All the geospatial vector magic happens here
      .data(feature(WorldJSON, WorldJSON.objects.subunits).features)
      // For each topographic feature appent a path object
      .enter()
      .append("path")
      .attr("d", world_path)
      .style("fill", (d) => {
        if (d.id == 'IDN') {
          return '#AE292D';
        } else {
          return '#F4E2E3'
        }
      })
      .style("stroke-width", 0.5)
      .style("stroke", "white")

    let self = this

    if (!this.state.projectionReady)
      this.setState({projectionReady: true})
  }

  render() {
    let animator = this.state.projectionReady ? this.props.children : null

    return (
      <div>
        <div id="map-wrapper" className="map-wrapper" style={{marginTop: this.props.marginTop}}></div>
        { animator }
      </div>
    )
  }
}
