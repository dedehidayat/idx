import React, { Component } from 'react'
import { geoPath, select, geoEquirectangular, mouse } from 'd3'
import { feature } from 'topojson'

import SVGWorld from 'json!./countries.topojson'

export default class MapOverlay extends Component {
  constructor() {
    super()

    this.data = [{
      name: "San Francisco, US",
      lat: 37.7749,
      lon: 122.4194,
      buy: 10,
      sell: 2
    }]
  }

  componentDidMount() {
    this.draw()
  }

  componentWillMount() {
    this.updateDimensions()
  }

  draw() {
    var projection = geoEquirectangular()
    .scale(this.state.height / Math.PI)
    .translate([this.state.width / 2, this.state.height / 2])
    .rotate([-162,0]);

    var svg1 = select("#w").append("svg")
      .attr("width", this.state.width)
      .attr("height", this.state.height);

      var world_path = geoPath()
          .projection(projection);

    svg1
      .selectAll("path")
      // All the geospatial vector magic happens here
      .data(feature(SVGWorld, SVGWorld.objects.subunits).features)
      // For each topographic feature appent a path object
      .enter()
      .append("path")
      .attr("d", world_path)
      .style("fill", (d) => {
        if (d.id == 'IDN') {
          return '#FF5656';
        } else {
          return '#FFCCCC'
        }
      })
      .style("stroke-width", 0.5)
      .style("stroke", "white")

    svg1.on("mousedown.log", function() {
      console.log(mouse(this), 1);
      console.log('X Y: ', projection([106.8227, 6.1745]))
    });
  }

  updateDimensions() {
    this.setState({
      width: window.innerWidth,
      height: window.innerHeight
    });
  }

  render() {
    return (
      <div>
        <div className="world-overlay">
          <div id="w"></div>
        </div>
        { this.props.children }
      </div>
    )
  }
}
