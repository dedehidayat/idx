import React, { Component } from 'react'
import _ from 'lodash'
import AnimateOnChange from 'react-animate-on-change'

const color = {
	buy: 'rgb(57, 144, 59)',
	sell: 'rgb(160, 37, 40)',
	percent: 'rgb(0, 128, 255)'
}

export default class BuySell extends Component {
	static contextTypes = {
		projection: React.PropTypes.func.isRequired
	}

	constructor(props) {
		super()
	}

	parseNumber(number) {
    var precision = this.props.precision || 0

		return parseFloat(number).toFixed(precision).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
	}

	getPositionFromProjection(point) {
		let pos = this.context.projection([point.lon, point.lat])

		if (!pos) return null

			return {
				x: pos[0],
				y: pos[1]
			}
		}

	render() {
		const end = this.getPositionFromProjection(this.props.target)

		let objects = _.map(this.props.data, (point) => {
			const start = this.getPositionFromProjection(point)
			const styles = {
				position: 'absolute',
				top: start.y - 40,
				left: start.x - 125,
				textAlign: 'center',
				display: 'block',
				width: 250,
				fontWeight: 'bold',
				color: color.sell,
				fontSize: '16px',
				lineHeight: 1
			}

			if (point.labelPositioning) {
				styles.top += point.labelPositioning.top
				styles.left += point.labelPositioning.left
			}

			return (
				<span key={start.x + end.x} style={styles}>
					<span style={{
						display: 'block',
						marginLeft: (styles.width / 2) / 2,
						marginBottom: 10
					}}>
					<span style={{
						display: 'block',
						width: 125,
						backgroundColor: color.percent,
						padding: 8,
						color: 'white',
						textAlign: 'center',
						opacity: point.percent.change ? 1 : 0.5,
           				textShadow: "1px 1px 2px rgba(0,0,0,0.8)"
						}}>
						<AnimateOnChange
							baseClassName="block"
							animationClassName="flash animated"
							animate={point.buy.change != 0}>
								{ this.parseNumber(point.percent.value) }
						</AnimateOnChange>
					 </span>
					<span style={{
						display: 'block',
						width: 125,
						backgroundColor: color.buy,
						padding: 8,
						color: 'white',
						textAlign: 'center',
						opacity: point.buy.change ? 1 : 0.5,
            textShadow: "1px 1px 2px rgba(0,0,0,0.8)"
					}}>
						<AnimateOnChange
						baseClassName="block"
						animationClassName="flash animated"
						animate={point.buy.change != 0}>
						{ this.parseNumber(point.buy.value) }
						</AnimateOnChange>
					</span>
						<span style={{
							display: 'block',
							width: 125,
							backgroundColor: color.sell,
							padding: 8,
							color: 'white',
							textAlign: 'center',
							opacity: point.sell.change ? 1 : 0.5,
              textShadow: "1px 1px 2px rgba(0,0,0,0.8)"
						}}>
							<AnimateOnChange
								baseClassName="block"
								animationClassName="flash animated"
								animate={point.sell.change != 0}>
									{ this.parseNumber(point.sell.value) }
							</AnimateOnChange>
						</span>
					</span>
					<span style={{display: 'block', margin: '0 auto', width: '70%' }}>
						<span style={{
							display: 'block',
							height: 35,
							verticalAlign: 'middle',
							textTransform: 'uppercase',
              width: '100%',
              textAlign: 'center'
						}}>
							{point.name}
						</span>
					</span>
				</span>
			)
		})

		return (
			<div>
				{ objects.map((obj, i) => {
					return obj
				})}
			</div>
		)
	}
}
