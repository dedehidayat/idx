import React, { Component } from 'react'
import _ from 'lodash'
import AnimateOnChange from 'react-animate-on-change'
import config from './../../../utils/config'

export default class Nett extends Component {
	static contextTypes = {
		projection: React.PropTypes.func.isRequired
	}

	constructor(props) {
		super()
	}

	parseNumber(number) {
		return parseFloat(number).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
	}

	getPositionFromProjection(point) {
		let pos = this.context.projection([point.lon, point.lat])

		if (!pos) return null

			return {
				x: pos[0],
				y: pos[1] + 40
			}
		}

	render() {
		const end = this.getPositionFromProjection(this.props.target)

		let objects = _.map(this.props.data, (point) => {
			const start = this.getPositionFromProjection(point)
			const styles = {
				position: 'absolute',
				top: start.y,
				left: start.x - 100,
				textAlign: 'center',
				display: 'block',
				width: 200,
				fontWeight: 'bold',
				color: config.color.sell,
				fontSize: '12px',
				lineHeight: 1,
				opacity: point.change != 0 ? 1 : 0.5
			}

			if (point.labelPositioning) {
				styles.top += point.labelPositioning.top
				styles.left += point.labelPositioning.left
			}

			return (
				<span key={start.x + end.x} style={styles}>
					<span style={{
						display: 'block',
						marginLeft: (styles.width / 2) / 2,
						marginBottom: 5
					}}>
					<span style={{
						display: 'block',
						width: 100,
						backgroundColor: point.change < 0 || point.value < 0 ? config.color.sell : config.color.buy,
						padding: 8,
						color: 'white',
						textAlign: 'center'
					}}>
						<AnimateOnChange
						baseClassName="block"
						animationClassName="flash animated"
						animate={point.change != 0}>
						{ this.parseNumber(Math.abs(point.value)) }
						</AnimateOnChange>
					</span>
					</span>
					<span style={{ display: 'block' }}>
						<span style={{
							display: 'table-cell',
							width: 200,
							verticalAlign: 'middle',
							textTransform: 'uppercase'
						}}>
							{point.name}
						</span>
					</span>
				</span>
			)
		})

		return (
			<div>
				{ objects.map((obj, i) => {
					return obj
				})}
			</div>
		)
	}
}
