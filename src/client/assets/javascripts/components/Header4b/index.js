import React, { Component } from 'react'
import moment from 'moment-timezone'
import _ from 'lodash'
import AnimateOnChange from 'react-animate-on-change'

import './styles.scss'
import logo from './../../../images/idx.png'

class HDesc extends Component {
  render() {
    return(
      <hgroup className="hdesc">
        <h4 className="sublabel buy">
          <span className="status">total buy</span>
          <strong className="snominal">
            <span className="currency">
              <em>idr</em>
              mio
            </span>
            <span className="nominal">
                { this.props.totalBuyValue }
            </span>
          </strong>
        </h4>

        <h4 className="sublabel sell">
          <span className="status">total sell</span>
          <strong className="snominal">
            <span className="currency">
              <em>idr</em>
              mio
            </span>
            <span className="nominal">
                { this.props.totalSellValue }
            </span>
          </strong>
        </h4>
		
		<h4 className="sublabel grandtotal">
          <span className="status">grand total</span>
          <strong className="snominal">
            <span className="currency">
              <em>idr</em>
              mio
            </span>
            <span className="nominal">
                { this.props.grandtotal }
            </span>
          </strong>
        </h4>
      </hgroup>
    )
  }
}

export default class InfoNav extends Component {

  constructor(props) {
    super(props)

    this.state = {
      timeWIB: moment().tz('Asia/Jakarta').format('HH:mm'),
      value: props.value
    }
  }

  componentDidMount() {
    this.timer = setInterval(() => {
      this.updateDateTime()
    }, 1000)
  }

  componentWillUnmount() {
    clearInterval(this.timer)
    this.timer = null
  }

  updateDateTime() {
    this.setState({
      timeWIB: moment().tz('Asia/Jakarta').format('HH:mm')
    })
  }

  updateValue() {
    this.setState({
      value: (parseFloat(this.state.value) + 0.002).toFixed(4)
    })
  }

  render() {

    const legends = (this.props.legends || []).map((v, i) => {
      return (<li key={i} className={v.class}>{v.name}</li>)
    })

    return (
      <div className="row">

        <div className="infonavbar">
          <div className="cpthead">
            <h3 className="hcpt htime">{ this.state.timeWIB }</h3>
          </div>
        </div>

        <div className="trscaption">
          <div>
            <div className="caption">
              <h1><strong>{ this.props.title }</strong></h1>

              { this.props.children &&
                <div className="subcaption">
                  { this.props.children }
                </div>
              }

              {this.props.showInfo &&
                <HDesc totalBuyValue={this.props.totalBuyValue || 0} totalSellValue={this.props.totalSellValue || 0} />
              }
            </div>
          </div>
        </div>

        <div className="slogo">
          <img src={logo} className="img-responsive" alt="logo 4" />
        </div>

      </div>
    )
  }

}
