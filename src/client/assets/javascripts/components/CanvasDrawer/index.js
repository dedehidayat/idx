import React, { Component } from 'react'
import AnimateOnChange from 'react-animate-on-change'
import 'animate.css/animate.css'

const color = {
  buy: '#57ba59',
  sell: '#C02C30'
}

const legendItemWith = 35
const legendFontSize = 14

export class PointLegend extends Component {

  constructor() {
    super()
  }

  parseNumber(number) {
    return parseFloat(number).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
  }

  componentWillReceiveProps() {
  }

  render() {
    const { point, start, end } = this.props

    this.styles = {
      position: 'absolute',
      top: start.y - 40,
      left: start.x - 100,
      textAlign: 'center',
      display: 'block',
      width: 200,
      fontWeight: 'bold',
      color: color.sell,
      fontSize: '16px',
      lineHeight: 1
    }

    if (point.labelPositioning) {
      this.styles.top += point.labelPositioning.top
      this.styles.left += point.labelPositioning.left
    }

    return (
      <span key={start.x + end.x} style={this.styles}>
        <span style={{
          display: 'block',
          marginLeft: (this.styles.width / 2) / 2,
          marginBottom: 10
        }}>
          <span style={{ display: 'block', width: 100, backgroundColor: color.buy, padding: 8, color: 'white', textAlign: 'center' }}>
            <AnimateOnChange
              baseClassName="block"
              animationClassName="flash animated"
              animate={point.buy.change != 0}>
                { this.parseNumber(point.buy.value) }
            </AnimateOnChange>
          </span>
          <span style={{ display: 'block', fontSize: 13, width: 100, backgroundColor: '#000000', padding: 2, color: '#FFFFFF', textAlign: 'center' }}>IDR million</span>
          <span style={{ display: 'block', width: 100, backgroundColor: color.sell, padding: 8, color: 'white', textAlign: 'center' }}>
            <AnimateOnChange
              baseClassName="block"
              animationClassName="flash animated"
              animate={point.sell.change != 0}>
                { this.parseNumber(point.sell.value) }
            </AnimateOnChange>
          </span>
        </span>
        <span style={{ display: 'block' }}>
          <span style={{
            display: 'table-cell',
            width: 200,
            height: 35,
            verticalAlign: 'middle',
            textTransform: 'uppercase'
          }}>{point.name}</span>
        </span>
      </span>
    )
  }
}

export function drawCurvedLine(ctx, start, end, control, point, inverse) {
  ctx.beginPath()
  ctx.moveTo(start.x, start.y)
  ctx.quadraticCurveTo(control.x, control.y, end.x, end.y)
  ctx.lineWidth = point.volume ? point.volume : 1,
  ctx.strokeStyle = typeof inverse == 'undefined' ? color.buy : color.sell
  ctx.stroke()
}

export function drawArrow(ctx, start, control, end, point, T, inverse) {
  if (T >= 1 || T <= 0.05) return

  ctx.save();

  let x = Math.pow(1-T,2) * start.x + 2 * (1-T) * T * control.x + Math.pow(T,2) * end.x;
  let y = Math.pow(1-T,2) * start.y + 2 * (1-T) * T * control.y + Math.pow(T,2) * end.y;

  let c2x = control.x + (end.x - control.x) * T;
  let c2y = control.y + (end.y - control.y) * T;

  let dx = c2x - x
  let dy = c2y - y

  let dist = Math.sqrt((c2x - x) * (c2x - x) + (c2y - y) * (c2y - y));
  let angle = Math.acos((c2y - y) / dist);

  if (c2x < x) angle = 2 * Math.PI - angle;

  let arrowWidth = point.volume ? point.volume + 1 : 2;

  ctx.lineWidth = 1;
  ctx.beginPath();
  ctx.translate(x, y);
  ctx.rotate(-angle);
  ctx.fillStyle = typeof inverse == 'undefined' ? '#3E7B40' : color.sell;
  ctx.lineWidth = typeof inverse == 'undefined' ? 2 : 1;
  ctx.strokeStyle = typeof inverse == 'undefined' ? '#3E7B40' : color.sell;
  ctx.moveTo(0, -arrowWidth);
  ctx.lineTo(-arrowWidth, -arrowWidth);
  ctx.lineTo(0, 0);
  ctx.lineTo(arrowWidth, -arrowWidth);
  ctx.lineTo(0, -arrowWidth);
  ctx.closePath();
  ctx.fill();
  ctx.stroke();
  ctx.restore();
}

export function baseCircle(ctx, x, y, radius) {
  ctx.beginPath()
  ctx.arc(x, y, radius + 4, 0, Math.PI*180)
  ctx.closePath()
  ctx.fillStyle = '#fff'
  ctx.strokeStyle = color.buy
  ctx.lineWidth = 4
  ctx.fill()
  ctx.stroke()
}
