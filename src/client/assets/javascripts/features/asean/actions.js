import { CallAPI } from './../../utils/helper'

export function dataFetched(payload) {
	return {
		type: 'ASEAN_DATA_FETCHED',
		payload: payload
	}
}

export function updateData() {
  return (dispatch, getState) => {
    return CallAPI('world_major_bourse_indexs/sync', getState().asean).then((data) => {
      dispatch(dataFetched(data.data.data))
    })
  }
}
