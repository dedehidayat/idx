import React, { Component } from 'react'
import AnimateOnChange from 'react-animate-on-change'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import config from './../../utils/config'
import * as actionCreators from './actions'
import Header from './../../components/Header'
import Footer from './../../components/Footer'
import ScreenBlokr from './../../components/ScreenBlokr'
import { numberFormat } from './../../utils/helper'
import './styles.scss'

/**
 * LQ45View [Page 6]
 */
@connect(
  state => ({
    LQ45: state.LQ45
  }),
  dispatch => ({
    actions: bindActionCreators(actionCreators, dispatch)
  })
)
export default class LQ45View extends Component {

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  }

  componentDidMount() {
    this.pageTransition = setTimeout(() => {
      this.context.router.push('/')
    }, config.delayPerPage)

    this.props.actions.updateData()

    this.updateDataInterval = setInterval(() => {
      this.props.actions.updateData()
    }, config.dataUpdateInterval)

  }

  componentWillUnmount() {
    window.clearTimeout(this.pageTransition)
    window.clearTimeout(this.updateDataInterval)
    this.pageTransition = undefined
    this.updateDataInterval = undefined
  }

  // getPercentage(record) {
  //   record.previous_value = parseFloat(record.previous_value).toFixed(2);
  //   record.current_value = parseFloat(record.current_value).toFixed(2);
  //   if (record.previous_value == 0 && record.current_value == 0) {
  //     return 0;
  //   } else if (record.previous_value == 0 && record.current_value > 0) {
  //     return 100;
  //   } else {
  //     return (record.current_value / record.previous_value) * 100 - 100;
  //   }
  // }

  getStatusClass(record) {
    return record.percentage == 0 ? 'hblue' : (record.percentage > 0 ? 'hgreen' : 'hred');
  }

  // getDiff(record) {
  //   record.previous_value = parseFloat(record.previous_value).toFixed(2);
  //   record.current_value = parseFloat(record.current_value).toFixed(2);
  //   return (record.current_value - record.previous_value).toFixed(2);
  // }

  render() {
    return(
      <div className="wrapper">
        <div className="section-top">
          <Header
            title="LQ 45"
            showInfo={false}>
              <div className="innerscaption threeoption">
                <label className="btn-group" data-toggle="buttons">
                  <span className="btn btn-success">
                    <input type="checkbox" />
                    <span className="glyphicon glyphicon-ok"></span>
                  </span>
                  <span className="labelnav">UP</span>
                </label>

                <label className="btn-group" data-toggle="buttons">
                  <span className="btn btn-primary">
                    <input type="checkbox" />
                    <span className="glyphicon glyphicon-ok"></span>
                  </span>
                  <span className="labelnav">UNCHANGED</span>
                </label>

                <label className="btn-group" data-toggle="buttons">
                  <span className="btn btn-danger">
                    <input type="checkbox" />
                    <span className="glyphicon glyphicon-ok"></span>
                  </span>
                  <span className="labelnav">DOWN</span>
                </label>
                <div className="clear"></div>
              </div>
          </Header>

          <div className="mapsection">
            <div className="innercontent">
              <div className="container">
                <div className="cnt-lq">
                  { this.props.LQ45.data.sort((a, b)=>{
                      return a.percentage > b.percentage ? -1 : ((a.percentage == b.percentage) ? 0 : 1);
                  }).map((data, i) => {
                    const arrow = (data.percentage == 0)
                      ? <i className="fa fa-minus-circle" style={{marginRight: 1}}></i>
                      : (data.percentage > 0)
                        ? <i className="fa fa-chevron-circle-up" style={{marginRight: 1}}></i>
                        : <i className="fa fa-chevron-circle-down" style={{marginRight: 1}}></i>

                    return (
                      <span key={i} style={{ marginBottom: '6px !important' }} className={ 'ilq ' + (this.getStatusClass(data)) }>
                        <div style={{ padding: '15px 0' }}>{data.name}</div>
                        <div className='pPoint'>
                          <span className='pointLeft'>
                            <i className="fa" style={{marginRight: 13}}></i> { numberFormat(data.current_value, 0) }
                          </span>
                          <span className='pointRight'>
                            {arrow} { numberFormat(data.percentage, 2) } %
                          </span>
                          <div className="clearfix"></div>
                        </div>
                      </span>
                    )
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
        <ScreenBlokr />
      </div>
    )
  }
}
