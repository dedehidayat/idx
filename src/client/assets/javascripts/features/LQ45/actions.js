import { CallAPI } from './../../utils/helper'

export function dataFetched(payload) {
	return {
		type: 'LQ45_DATA_FETCHED',
		payload: payload
	}
}

export function updateData(payload) {
	return (dispatch, getState) => {
	    return CallAPI('lqs/sync', getState().foreign.data).then((response) => {
	    	dispatch(dataFetched(response.data))
	    })
	}
}