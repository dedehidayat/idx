import { CallAPI } from './../../utils/helper'

export function dataFetched(payload) {
	return {
		type: 'PROVINCE_DATA_FETCHED',
		payload: payload
	}
}

export function updateData(region) {
	return (dispatch, getState) => {
    return CallAPI(`provinces/sync?side=${region}`, getState().province.data).then((data) => {
      dispatch(dataFetched(data.data.data))
    })
  }
}
