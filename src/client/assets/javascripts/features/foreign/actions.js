import { CallAPI } from './../../utils/helper'

export function dataFetched(payload) {
	return {
		type: 'FOREIGN_DATA_FETCHED',
		payload: payload
	}
}

export function updateData(payload) {
	return (dispatch, getState) => {
    return CallAPI('foreigns/sync', getState().foreign.data).then((data) => {
      dispatch(dataFetched(data.data.data))
    })
  }
}
