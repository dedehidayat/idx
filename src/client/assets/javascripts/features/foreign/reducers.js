import { calculateVolume } from './../../utils/helper'

const update = (state, mutations) => Object.assign([], state, mutations)

function randomizeBuyValue() {
    return (Math.random() * (10 - 5) + 5) * 10000
  }

function randomizeSellValue() {
  return (Math.random() * (5 - 1) + 1) * 10000
}

const initialState = {
  buy: {
    min: 0,
    max: 0
  },
  sell: {
    min: 0,
    max: 0,
  },
  data: []
}

export default function reducers(state = initialState, action) {
	switch (action.type) {
		case 'FOREIGN_DATA_FETCHED':
			var collections = _.map(action.payload, (o) => {
        return {
          buy: o.buy.value,
          sell: o.sell.value
        }
      })

      var buy = {
        max: _.maxBy(collections, 'buy')['buy'],
        min: _.minBy(collections, 'buy')['buy']
      }

      var sell = {
        max: _.maxBy(collections, 'sell')['sell'],
        min: _.minBy(collections, 'sell')['sell']
      }

      return Object.assign({}, state, {
        ...state,
        buy: buy,
        sell: sell,
        data: action.payload
      })
			break
		default:
			return state
			break
	}
}
