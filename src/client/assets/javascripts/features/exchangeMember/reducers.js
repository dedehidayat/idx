const update = (state, mutations) => Object.assign({}, state, mutations)

const initialState = {
  meta: {
    buy_summary: 0,
    sell_summary: 0
  },
  data: []
}

export default function reducers(state = initialState, action) {
	switch (action.type) {
		case 'EXCHANGE_MEMBER_DATA_FETCHED':
		case 'EXCHANGE_MEMBER_DATA_CHANGED':
			return update(state, action.payload)
			break
		default:
			return state
			break
	}
}