import { CallAPI } from './../../utils/helper'

export function dataFetched(payload) {
	return {
		type: 'DOMESTIC_DATA_FETCHED',
		payload: payload
	}
}

export function updateData() {
	return (dispatch, getState) => {
    return CallAPI('domestics/sync', getState().domestic.data).then((data) => {
      dispatch(dataFetched(data.data.data))
    })
  }
}
