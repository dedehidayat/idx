import React, { Component } from 'react'
import Header from './../../components/Header'
import Footer from './../../components/Footer'
import ScreenBlokr from './../../components/ScreenBlokr'

import config from './../../utils/config'
import logo from './../../../images/idx-logo.png'

export default class HomeView extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.pageTransition = setTimeout(() => {
      this.context.router.push('/2')
    }, config.delayPerPageMin)
  }

  componentWillUnmount() {
    window.clearTimeout(this.pageTransition)
    this.pageTransition = undefined
  }

  render() {
    return(
      <div className="homepage wrapper">
        <div className="section-top">
          <Header title="IDX Visual Trading" value={6345.77}/>
        </div>
        <ScreenBlokr />
      </div>
    )
  }
}

HomeView.contextTypes = {
  router: React.PropTypes.object.isRequired
}
