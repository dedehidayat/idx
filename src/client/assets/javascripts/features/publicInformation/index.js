import React, { Component } from 'react'

export default class PublicInformation extends Component {
	render() {
		return (
			<div className="sectionadrs">
			   <div className="row">
			      <div className="col-md-2">
			         <div className="location">
			            <h4>Pusat Informasi Go Public</h4>
			            <p>
			               Gedung Bursa Efek Indonesia<br />
			               Tower 2 Lantai Dasar<br />
			               Jl. Jend. Sudirman Kav. 52 – 53 Jakarta<br />
			               Telp : (021) 515 4155<br />
			               E-mail : gopublic@idx.co.id; paramita@idx.co.id<br />
			               Website : www.gopublic.idx.co.id<br />
			               Contact Person : Paramita Sari<br />
			               No. HP : 081806010894<br />
			               Contact Person : Sofiyan Adhi Kusumah<br />
			               No. HP : 082121159834
			            </p>
			            <h4>1. Pusat Informasi Go Public Banda Aceh</h4>
			            <p>Jl. Tengku Imeum Leung Bata<br />No. 84, Banda Aceh 23247 - Aceh<br />Contact Person : Thasrif Murhadi<br />No. Telp : (0651) 35101 / HP : 08116851505<br />Email : thasrif.murhadi@idx.co.id<br />
			               Website : www.gopublic.idx.co.id<br />
			               Contact Person : Paramita Sari<br />
			               No. HP : 081806010894<br />
			               Contact Person : Sofiyan Adhi Kusumah<br />
			               No. HP : 082121159834
			            </p>
			            <h4>2. Pusat Informasi Go Public Medan</h4>
			            <p>Jl. Asia No. 182, Medan - Sumatra Utara<br />Contact Person : M. Pintor Nasution<br />No. Telp : (061) 733 2920 / HP : 081311416320<br />Email : pintor.nasution@idx.co.id</p>
			         </div>
			      </div>
			      <div className="col-md-2">
			         <div className="location">
			            <h4>3. Pusat Informasi Go Public Riau</h4>
			            <p>Jl. Jenderal Sudirman No. 73<br />(Sudirman Bawah), Pekanbaru - Riau<br />Contact Person : Emon Sulaeman<br />No. Telp : (0761) 848 414/ 839 529 / HP :<br />08117530660<br />Email : emon.sulaeman@idx.co.id</p>
			            <h4>4. Pusat Informasi Go Public Padang</h4>
			            <p>Jl. Pondok No. 90 A, Padang 25211 - Sumatra Barat<br />
			               Contact Person : Reza Sadat<br />
			               No. Telp : (0751) 811 330 / HP : 08116610479<br />
			               Email : reza.shahmeini@idx.co.id
			            </p>
			            <h4>5. Pusat Informasi Go Public Batam</h4>
			            <p>
			               Komplek Mahkota Raya Blok A No. 11, <br />
			               Jl. Engku Putri - Batam Center <br />
			               29456 Batam - Kepulauaan Riau <br />
			               Contact Person : Marco Poetra Kawet <br />
			               No. Telp : (0778) 748 3348 / HP : 08117705345 <br />
			               Email : marco.kawet@idx.co.id
			            </p>
			            <h4>6. Pusat Informasi Go Public Jambi</h4>
			            <p>
			               Jl. Kolonel Abun Jani No.11A dan 11B, <br />
			               Kel. Selamat, Kec. Telanaipura, Kota Jambi <br />
			               Contact Person : Gusti Bagus Ngurah Putra Sandiana <br />
			               No. Telp : (0741) 65788 / HP : 087883651575 <br />
			               Email : ngurah.sandiana@idx.co.id
			            </p>
			         </div>
			      </div>
			      <div className="col-md-2">
			         <div className="location">
			            <h4>7. Pusat Informasi Go Public Palembang</h4>
			            <p>
			               Jl. Angkatan 45, No. 13-14, RT 0014/ <br />
			               RW 004, Kel. Demang Lebar Daun,<br />
			               Kec. Ilir Barat I, Kota Palembang<br />
			               Contact Person : Early Saputra<br />
			               No. Telp : (0711) 564 9259 / HP : 081374908326<br />
			               Email : early.saputra@idx.co.id
			            </p>
			            <h4>8. Pusat Informasi Go Public Lampung</h4>
			            <p>
			               Jl. Jend. Sudirman No. 5D, <br />
			               Bandar Lampung 35118 <br />
			               Contact Person : Hendi Prayogi <br />
			               No. Telp : (0721) 260 188 / HP : 085274905111<br />
			               Email : hendi.prayogi@idx.co.id
			            </p>
			            <h4>9. Pusat Informasi Go Public Bandung</h4>
			            <p>
			               Jl. Veteran No. 10, Bandung<br />
			               Contact Person : Hari Mulyono<br />
			               No. Telp : (022) 421 4349 / HP : 08877729438<br />
			               Email : hari.mulyono@idx.co.id
			            </p>
			            <h4>10. Pusat Informasi Go Public Semarang</h4>
			            <p>
			               Jl. M. H. Thamrin No. 152, Semarang <br />
			               Jawa Tengah <br />
			               Contact Person : Stephanus Cahyanto Kristiadi <br />
			               No. Telp : (024) 844 6878 / HP : 087887736376 <br />
			               Email : stephanus.kristiadi@idx.co.id
			            </p>
			         </div>
			      </div>
			      <div className="col-md-2">
			         <div className="location">
			            <h4>11. Pusat Informasi Go Public Yogyakarta</h4>
			            <p>
			               Jl. P. Mangkubumi No. 111, <br />
			               Yogyakarta 55232 <br />
			               Contact Person : Irfan Noor Riza <br />
			               No. Telp : (0274) 587 457 / HP : 08158301674 <br />
			               Email : irfan.noor@idx.co.id
			            </p>
			            <h4>12. Pusat Informasi Go Public Surabaya</h4>
			            <p>
			               Jl. Basuki Rachmat No. 46, Surabaya 60261<br />
			               Contact Person : Dewi Sriana Rihantyasni<br />
			               No. Telp : (031) 534 0888 / HP : 081357081010<br />
			               Email : dewi.sriana@idx.co.id
			            </p>
			            <h4>13. Pusat Informasi Go Public Denpasar</h4>
			            <p>
			               Jl. P.B. Sudirman 10 X Kav. 2,<br />
			               Denpasar, Bali<br />
			               Contact Person : I Gusti Agung Alit Nityaryana<br />
			               No. Telp : (0361) 256 701 / HP : 081239533225<br />
			               Email : alit@idx.co.id
			            </p>
			            <h4>14. Pusat Informasi Go Public Pontianak</h4>
			            <p>
			               Komplek Perkantoran Central Perdana<br />
			               Blok A2-A3, Jl. Perdana -<br />
			               Kota Pontianak, 78124<br />
			               Contact Person : Taufan Febiola<br />
			               No. Telp : (0561) 734 112 / HP : 0816713753<br />
			               Email : taufan.febiola@idx.co.id
			            </p>
			         </div>
			      </div>
			      <div className="col-md-2">
			         <div className="location">
			            <h4>15. Pusat Informasi Go Public Banjarmasin</h4>
			            <p>
			               Jl. Ahmad Yani, Kilometer (PAL) 1,5 .No. 103,<br />
			               Banjarmasin<br />
			               Contact Person : M. Wira Adibrata<br />
			               No. Telp : (0511) 325 6985 / HP : 08114811984<br />
			               Email : adibrata@idx.co.id
			            </p>
			            <h4>16. Pusat Informasi Go Public Balikpapan</h4>
			            <p>
			               Jl. Jend. Sudirman No. 33B, Balikpapan <br />
			               Kalimantan Timur <br />
			               Contact Person : Dinda Ayu Amalliya <br />
			               No. Telp : (0542) 421 555 / HP : 081346604080 <br />
			               Email : dinda.amalliya@idx.co.id <br />
			               17. Pusat Informasi Go Public Makassar <br />
			               Jl. Dr. Sam Ratulangi No. 124, Makassar <br />
			               Contact Person : Fahmin Amirullah <br />
			               No. Telp : (0411) 891 0124 / HP :081342171871 <br />
			               Email : fahmin.amirullah@idx.co.id
			            </p>
			            <h4>17. Pusat Informasi Go Public Manado</h4>
			            <p>
			               Ruko Mega Style Blok 1C No.9,<br />
			               Kompleks Mega Mas, Jl. Piere Tendean,<br />
			               Boulevard, Manado 95111<br />
			               Contact Person : Fonny The<br />
			               No. Telp : (0431) 888 1166 / HP : 08124429088<br />
			               Email : fonny@idx.co.id
			            </p>
			         </div>
			      </div>
			      <div className="col-md-2">
			         <div className="location">
			            <h4>18. Pusat Informasi Go Public Jayapura</h4>
			            <p>
			               Komplek Jayapura Pasifik Permai <br />
			               Blok H No.19, Jayapura 99112, Papua <br />
			               Contact Person : Kresna Aditya Payokwa <br />
			               No. Telp : (0967) 532 414/ 532 430 / HP : <br />
			               081344566369 <br />
			               Email : kresna.aditya@idx.co.id
			            </p>
			            <h4>19. Pusat Informasi Go Public Kendari</h4>
			            <p>
			               Jl. Syekh Yusuf No.20, Kota Kendari<br />
			               Contact Person : Epha Karunia T<br />
			               No. Telp : (0401) 3131266 / HP : 081220042818<br />
			               Email : epha.karunia@idx.co.id
			            </p>
			         </div>
			      </div>
			   </div>
				</div>
			</div>
		)
	}
} 